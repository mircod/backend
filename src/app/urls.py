from django.urls import path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.routers import SimpleRouter
from rest_framework_nested.routers import NestedSimpleRouter

from app.views import (
    register_view,
    login_view,
    UserView,
    UserMeasurementView,
    DeviceView,
    DeviceMeasurementView,
    create_superuser_view,
)

router = SimpleRouter()

router.register("user", UserView, "user")
router.register("device", DeviceView, "device")
temp_router = NestedSimpleRouter(router, "user", lookup="user")
temp_router.register("temp", UserMeasurementView, "temp")
measure_router = NestedSimpleRouter(router, "device", lookup="device")
measure_router.register("temp", DeviceMeasurementView, "temp")
urlpatterns = [
    path("auth/register/", register_view, name="registration"),
    path("auth/login/", login_view, name="login"),
    path("admin/", create_superuser_view, name="create-superuser"),
    *router.urls,
    *temp_router.urls,
    *measure_router.urls,
]

schema_view = get_schema_view(
    openapi.Info(
        title="Bot api",
        default_version="v1",
        description="API description",
        terms_of_service="",
    ),
    public=True,
)

urlpatterns += [
    re_path(
        r"^swagger(?P<format>\.json|\.yaml)$",
        schema_view.without_ui(cache_timeout=0),
        name="schema-json",
    ),
    re_path(
        r"^swagger/$",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
]

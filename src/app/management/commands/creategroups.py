from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType

from app.models import Device, Measurement, User
from django.core.management import BaseCommand


class Command(BaseCommand):
    help = "Create group Doctor, group Patient and set permissions for this group"

    def handle(self, *args, **options):
        doctor_group, created = Group.objects.get_or_create(name="Doctor")
        patient_group, created = Group.objects.get_or_create(name="Patient")

        doctor_permission_names_list = ["view_user", "change_user", "delete_user", "view_device", "view_measurement"]
        patient_permission_names_list = [
            "add_device",
            "change_device",
            "delete_device",
            "add_measurement",
            "change_measurement",
            "delete_measurement",
        ]

        for model in [User, Device, Measurement]:
            content_type = ContentType.objects.get_for_model(model)
            model_permissions = Permission.objects.filter(content_type=content_type)
            for permission in model_permissions:
                if permission.codename in doctor_permission_names_list:
                    doctor_group.permissions.add(permission)
                if permission.codename in patient_permission_names_list:
                    patient_group.permissions.add(permission)

        print("Doctor group permissions:")
        for i in doctor_group.permissions.values_list("name"):
            print(i)
        print("\nPatient group permissions:")
        for i in patient_group.permissions.values_list("name"):
            print(i)

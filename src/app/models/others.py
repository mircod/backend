from django.db import models

from app.models import User


class Device(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="devices", verbose_name="Пациент")
    charge = models.IntegerField(verbose_name="Заряд")

    class Meta:
        verbose_name = "Устройство"
        verbose_name_plural = "Устройства"
        ordering = ("-id",)


class Measurement(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name="Пациент", default=1, related_name="measurements"
    )
    value = models.FloatField(verbose_name="Температура")
    date = models.DateTimeField(verbose_name="Дата измерения")
    device = models.ForeignKey(
        Device,
        on_delete=models.DO_NOTHING,
        related_name="measurements",
        verbose_name="Устройство",
    )

    class Meta:
        verbose_name = "Измерение"
        verbose_name_plural = "Измерения"
        ordering = ("-date",)

    def __str__(self):
        return str(self.value)

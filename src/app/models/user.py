from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.utils.translation import gettext_lazy as _


class UserManager(BaseUserManager):
    def create_user(self, username, first_name, last_name, password, **extra_fields):
        """
        Create and save a user with the given username, email, first_name, last_name and password.
        """
        if not username:
            raise ValueError(_("The given username must be set"))

        user = self.model(username=username, first_name=first_name, last_name=last_name, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, first_name, last_name, password, **extra_fields):
        """
        Create and save a superuser with the given username, email, first_name, last_name and password.
        """
        user = self.model(
            username=username, first_name=first_name, last_name=last_name, is_superuser=True, **extra_fields
        )
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):

    objects = UserManager()

    username = models.TextField(
        _("username"),
        unique=True,
        help_text=_("Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."),
        error_messages={
            "unique": _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(_("first name"), max_length=150)
    last_name = models.CharField(_("last name"), max_length=150)
    birth_date = models.DateField(null=True, blank=True)

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email"]

    def get_full_name(self):
        """
        Return the first_name plus the last_name plus patronymic, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")

from rest_framework.exceptions import APIException


class UsernameExistsError(APIException):
    status_code = 409
    default_detail = "Username already exists"

import re

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from app.models import User, Measurement, Device
from app.validators import password_validator


class RegistrationSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(min_length=8, validators=[password_validator], required=True)
    confirm_password = serializers.CharField(min_length=8, validators=[password_validator], required=True)

    def is_valid(self, raise_exception=False):
        super(RegistrationSerializer, self).is_valid(raise_exception)
        password = self.initial_data["password"]
        confirm_password = self.initial_data["confirm_password"]
        if password != confirm_password:
            raise ValidationError({"detail": "Passwords don't match"})


class RegisterUserResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username")


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()


class LoginResponseSerializer(serializers.Serializer):
    token = serializers.CharField()


class PatientSerializer(serializers.ModelSerializer):
    device_id = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    temperature = serializers.SerializerMethodField()
    temp_date = serializers.SerializerMethodField()
    charge = serializers.SerializerMethodField()

    def get_charge(self, obj):
        if obj.devices.first():
            return obj.devices.first().charge
        return None

    def get_temp_date(self, obj):
        if obj.measurements.first():
            return obj.measurements.first().date
        return ""

    def get_temperature(self, obj):
        if obj.measurements.first():
            return obj.measurements.first().value
        return ""

    def get_name(self, obj):
        return f"{obj.last_name} {obj.first_name}"

    def get_device_id(self, obj: User):
        if obj.devices.first():
            return obj.devices.first().id
        return None

    class Meta:
        model = Device
        fields = ("id", "device_id", "name", "temperature", "temp_date", "charge")


class MeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Measurement
        fields = ("id", "value", "date", "device", "user")
        read_only_fields = ("device", "user")


class PatientCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("first_name", "last_name", "username", "birth_date")


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ("id", "charge", "user")


class BulkDeleteUserSerializer(serializers.Serializer):
    id = serializers.ListField(child=serializers.IntegerField())


class DownloadUserSerializer(serializers.Serializer):
    id = serializers.ListField(child=serializers.IntegerField())
    content_type = serializers.CharField()

import factory

from app.models import User, Device, Measurement


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Faker("name")

    class Meta:
        model = User


class DeviceFactory(factory.django.DjangoModelFactory):
    charge = 100
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Device


class MeasurementFactory(factory.django.DjangoModelFactory):
    value = 36.6
    date = factory.Faker("date")
    device = factory.SubFactory(DeviceFactory)
    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Measurement

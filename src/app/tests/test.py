from rest_framework import status

from app.services import add_user_to_doctor_group, add_user_to_patient_group
from app.tests.factories import MeasurementFactory, DeviceFactory, UserFactory


def test_user_temp_ok(api_client, auth_headers, user):
    doctor = user
    add_user_to_doctor_group(doctor)
    measurement = MeasurementFactory()
    user_id = measurement.device.user.id
    response = api_client.get(f"/api/v1/user/{user_id}/temp/", **auth_headers)
    assert response.status_code == status.HTTP_200_OK


def test_user_temp_error(api_client, auth_headers, user):
    doctor = user
    add_user_to_doctor_group(doctor)
    MeasurementFactory()
    response = api_client.get(f"/api/v1/user/0/temp/", **auth_headers)
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_device_temp_ok(api_client, auth_headers, user):
    device = DeviceFactory(user=user)
    measurement = MeasurementFactory(device=device, user=user)
    add_user_to_patient_group(measurement.device.user)
    response = api_client.post(
        f"/api/v1/device/{device.id}/temp/",
        {"value": measurement.value, "date": measurement.date, "device": measurement.device.id},
        **auth_headers,
    )
    assert response.status_code == status.HTTP_201_CREATED


def test_device_temp_error(api_client, auth_headers, user):
    device = DeviceFactory(user=user)
    measurement = MeasurementFactory(device=device, user=user)
    add_user_to_patient_group(measurement.device.user)
    device_id = measurement.device.id
    response = api_client.post(
        f"/api/v1/device/{device_id+1}/temp/",
        {"value": measurement.value, "date": measurement.date, "device": device_id + 1},
        **auth_headers,
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND

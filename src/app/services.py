from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.db.models import QuerySet

from app.exceptions import UsernameExistsError
from app.models import User, Device, Measurement


def register_user(username: str, password: str, is_superuser=False) -> User:
    user, created = User.objects.get_or_create(username=username, is_superuser=is_superuser)
    if not created:
        raise UsernameExistsError()
    user.set_password(password)
    user.save()
    return user


def is_device_belongs_to_user(user: User, device_id: int) -> bool:
    user_devices = Device.objects.filter(user=user)
    return device_id in list(user_devices.values_list("id", flat=True))


def get_device_by_id(id: int) -> Device:
    return Device.objects.get(id=id)


def delete_users(ids: list[int]):
    User.objects.filter(id__in=ids).delete()


def add_user_to_doctor_group(user: User):
    doctor_group, created = Group.objects.get_or_create(name="Doctor")
    if created:
        set_doctor_permissions(doctor_group)
    doctor_group.user_set.add(user)


def set_doctor_permissions(doctor_group: Group):
    doctor_permission_names_list = ["view_user", "change_user", "delete_user", "view_device", "view_measurement"]

    for model in [User, Device, Measurement]:
        content_type = ContentType.objects.get_for_model(model)
        model_permissions = Permission.objects.filter(content_type=content_type)
        for permission in model_permissions:
            if permission.codename in doctor_permission_names_list:
                doctor_group.permissions.add(permission)


def add_user_to_patient_group(user: User):
    patient_group, created = Group.objects.get_or_create(name="Patient")
    if created:
        set_patient_permissions(patient_group)
    patient_group.user_set.add(user)


def set_patient_permissions(patient_group: Group):
    patient_permission_names_list = [
        "add_device",
        "change_device",
        "delete_device",
        "add_measurement",
        "change_measurement",
        "delete_measurement",
    ]

    for model in [User, Device, Measurement]:
        content_type = ContentType.objects.get_for_model(model)
        model_permissions = Permission.objects.filter(content_type=content_type)
        for permission in model_permissions:
            if permission.codename in patient_permission_names_list:
                patient_group.permissions.add(permission)


def get_users_by_id(ids: list) -> QuerySet:
    return User.objects.filter(id__in=ids).prefetch_related("devices", "measurements")


def get_users_list(users: QuerySet) -> list:
    users_list = []
    for user in users:
        d = {"first_name": user.first_name, "last_name": user.last_name}
        measurement = user.measurements.first() if user.measurements.first() else False
        device = measurement.device if measurement and measurement.device else False
        d["measurement_value"] = measurement.value if measurement else "no data"
        d["measurement_date"] = measurement.date if measurement else "-"
        d["device_id"] = device.id if device else "-"
        d["device_charge"] = device.charge if device else "-"
        users_list.append(d)
    return users_list

import re

from rest_framework.exceptions import ValidationError


def password_validator(password: str):
    password_match = re.match(r"^[A-Za-z0-9]*$", password)
    if not password_match:
        raise ValidationError({"detail": "Your password must contain nothing but letters and numbers"})

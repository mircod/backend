import csv

from django.contrib.auth import authenticate
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse
from django.utils.decorators import method_decorator

from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes, action
from rest_framework.exceptions import AuthenticationFailed, PermissionDenied
from rest_framework.filters import SearchFilter
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from app.models import User, Measurement, Device
from app.serializers import (
    RegistrationSerializer,
    RegisterUserResponseSerializer,
    LoginSerializer,
    LoginResponseSerializer,
    PatientSerializer,
    MeasurementSerializer,
    BulkDeleteUserSerializer,
    DeviceSerializer,
    PatientCreateSerializer,
    DownloadUserSerializer,
)
from app.services import (
    register_user,
    delete_users,
    is_device_belongs_to_user,
    get_device_by_id,
    add_user_to_doctor_group,
    add_user_to_patient_group,
    get_users_by_id,
    get_users_list,
)


@swagger_auto_schema(
    method="post",
    request_body=RegistrationSerializer(),
    responses={200: RegisterUserResponseSerializer()},
    operation_summary="Sign up.",
)
@api_view(["POST"])
def register_view(request):
    serializer = RegistrationSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    user = register_user(serializer.validated_data["username"], serializer.validated_data["password"])
    add_user_to_doctor_group(user)
    return Response(RegisterUserResponseSerializer(user).data)


@swagger_auto_schema(
    method="post",
    request_body=LoginSerializer(),
    responses={200: LoginResponseSerializer()},
    operation_summary="Sign in.",
)
@api_view(["POST"])
def login_view(request):
    serializer = LoginSerializer(data=request.data)
    serializer.is_valid(True)
    user = authenticate(request, **serializer.validated_data)
    if not user:
        raise AuthenticationFailed
    token, _ = Token.objects.get_or_create(user=user)
    return Response({"token": token.key})


@swagger_auto_schema(
    method="post",
    request_body=RegistrationSerializer(),
    responses={200: RegisterUserResponseSerializer()},
    operation_summary="Create admin.",
)
@api_view(["POST"])
def create_superuser_view(request):
    serializer = RegistrationSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    user = register_user(
        serializer.validated_data["username"], serializer.validated_data["password"], is_superuser=True
    )
    return Response(RegisterUserResponseSerializer(user).data)


@method_decorator(name="list", decorator=swagger_auto_schema(operation_summary="Returns user list"))
@method_decorator(
    name="retrieve",
    decorator=swagger_auto_schema(operation_summary="Get info about user"),
)
@method_decorator(name="destroy", decorator=swagger_auto_schema(operation_summary="Delete user"))
@method_decorator(name="partial_update", decorator=swagger_auto_schema(operation_summary="Update user info"))
@method_decorator(
    name="update", decorator=swagger_auto_schema(operation_summary="Update user info with sending all fields")
)
@method_decorator(name="retrieve", decorator=permission_required("app.view_user", raise_exception=True))
@method_decorator(name="list", decorator=permission_required("app.view_user", raise_exception=True))
@method_decorator(name="destroy", decorator=permission_required("app.delete_user", raise_exception=True))
@method_decorator(name="bulk_delete", decorator=permission_required("app.delete_user", raise_exception=True))
@method_decorator(name="update", decorator=permission_required("app.change_user", raise_exception=True))
@method_decorator(name="partial_update", decorator=permission_required("app.change_user", raise_exception=True))
class UserView(
    mixins.RetrieveModelMixin, mixins.ListModelMixin, mixins.DestroyModelMixin, mixins.UpdateModelMixin, GenericViewSet
):
    permission_classes = (IsAuthenticated,)
    serializer_class = PatientSerializer
    queryset = User.objects.filter(groups__name__contains="Patient").prefetch_related("measurements", "devices")
    filter_backends = [SearchFilter]
    search_fields = ["first_name", "last_name"]

    @swagger_auto_schema(
        methods=["delete"], request_body=BulkDeleteUserSerializer(), operation_summary="Delete a few users"
    )
    @action(methods=["DELETE"], detail=False)
    def bulk_delete(self, request, *args, **kwargs):
        serializer = BulkDeleteUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        delete_users(ids=serializer.validated_data["id"])
        return Response(status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(request_body=PatientCreateSerializer(), operation_summary="Create patient")
    def create(self, request, *args, **kwargs):
        serializer = PatientCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        patient = serializer.save()
        add_user_to_patient_group(patient)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(
        methods=["post"], request_body=DownloadUserSerializer(), operation_summary="Download info about users"
    )
    @action(methods=["POST"], detail=False)
    def download(self, request, *args, **kwargs):
        serializer = DownloadUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        ids = serializer.validated_data["id"]
        users = get_users_list(get_users_by_id(ids))
        content_type = serializer.validated_data["content_type"]

        if content_type == "csv":
            response = HttpResponse(
                content_type="text/csv",
                headers={"Content-Disposition": 'attachment; filename="patients.csv"'},
            )
            writer = csv.writer(response)
            for user in users:
                writer.writerow(
                    [
                        user["first_name"],
                        user["last_name"],
                        user["measurement_value"],
                        user["measurement_date"],
                        user["device_id"],
                        user["device_charge"],
                    ]
                )
            return response
        return Response(status=status.HTTP_404_NOT_FOUND)


@method_decorator(
    name="list",
    decorator=swagger_auto_schema(operation_summary="Get list of user temperature measurements"),
)
@method_decorator(name="list", decorator=permission_required("app.view_measurement", raise_exception=True))
class UserMeasurementView(mixins.ListModelMixin, GenericViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = MeasurementSerializer

    def get_queryset(self):
        user = get_object_or_404(User, id=self.kwargs["user_pk"])
        return Measurement.objects.filter(device__user=user)


@method_decorator(name="retrieve", decorator=swagger_auto_schema(operation_summary="Returns info about device"))
@method_decorator(name="create", decorator=swagger_auto_schema(operation_summary="Create device"))
@method_decorator(name="update", decorator=swagger_auto_schema(operation_summary="Update info about device"))
@method_decorator(name="list", decorator=swagger_auto_schema(operation_summary="Returns list of devices"))
@method_decorator(name="retrieve", decorator=permission_required("app.view_device", raise_exception=True))
@method_decorator(name="create", decorator=permission_required("app.add_device", raise_exception=True))
@method_decorator(name="update", decorator=permission_required("app.change_device", raise_exception=True))
@method_decorator(name="partial_update", decorator=permission_required("app.change_device", raise_exception=True))
class DeviceView(
    mixins.RetrieveModelMixin, mixins.CreateModelMixin, mixins.UpdateModelMixin, mixins.ListModelMixin, GenericViewSet
):
    serializer_class = DeviceSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Device.objects.all()

    def update(self, request, *args, **kwargs):
        user = request.user
        device_id = int(kwargs["pk"])
        device = get_object_or_404(Device, id=device_id)

        if is_device_belongs_to_user(user, device_id):
            return super().update(request, *args, **kwargs)
        return Response(status=status.HTTP_403_FORBIDDEN)


@method_decorator(name="create", decorator=swagger_auto_schema(operation_summary="Save measurement data from device"))
@method_decorator(name="create", decorator=permission_required("app.add_measurement", raise_exception=True))
class DeviceMeasurementView(mixins.CreateModelMixin, GenericViewSet):
    serializer_class = MeasurementSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        device = get_device_by_id(id=self.kwargs["device_pk"])
        serializer.save(device=device, user=device.user)

    def create(self, request, *args, **kwargs):
        device_id = int(kwargs["device_pk"])
        device = get_object_or_404(Device, id=device_id)
        if is_device_belongs_to_user(request.user, device_id) or self.request.user.is_superuser:
            return super().create(request, *args, **kwargs)
        return Response(status=status.HTTP_403_FORBIDDEN)

# Install project
- `cd src`
- `poetry install`
- `poetry shell`
- `python manage.py migrate`
- `python manage.py runserver`
